package com.example.mary.gauthapp.model;

import io.realm.RealmObject;

/**
 * Created by mary on 22.06.17.
 */

public class User extends RealmObject{

    private String name, lastname, email, phone;
    private Integer age;
    private Boolean hascat;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Boolean getHascat() {
        return hascat;
    }

    public void setHascat(Boolean hascat) {
        this.hascat = hascat;
    }
}
