package com.example.mary.gauthapp;

import android.app.Application;
import io.realm.Realm;


public class GauthApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
    }


}
